#!/bin/bash
WIN_INSTALL_DIR="D://Programs/NeoVim"
WIN_NEOVIM_ID="Neovim.Neovim"
WIN_HOME="C://Users/Herman"
WIN_NEOVIM_CONFIG="$WIN_HOME/AppData/Local/nvim/"
UNIX_NEOVIM_ID="neovim"
UNIX_HOME="/home/s0001821"
UNIX_NEOVIM_CONFIG="$UNIX_HOME/.config/nvim/"
IS_WIN=0
IS_UNIX=0
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo "Detected system - Linux"
    INSTALL_DIR="$UNIX_INSTALL_DIR"
    NEOVIM_CONFIG="$UNIX_NEOVIM_CONFIG"
    IS_UNIX=1
elif [[ "$OSTYPE" == "win32" || "$OSTYPE" == "msys" || "$OSTYPE" == "cygwin" ]]; then
    echo "Detected system - Windows"
    INSTALL_DIR="$WIN_INSTALL_DIR"
    NEOVIM_CONFIG="$WIN_NEOVIM_CONFIG"
    IS_WIN=1
fi

NEOVIM_CONFIG_PATH="${NEOVIM_CONFIG}init.lua"
NEOVIM_CONFIG_LUA_INIT="${NEOVIM_CONFIG}lua/init"
NEOVIM_CONFIG_LUA_POSTINIT="${NEOVIM_CONFIG}lua/postinit"

if [ $IS_UNIX -eq 1 ]; then
    #   check if program already installed; if so skip installation
    NONE="$(apt-cache policy neovim | grep "(none)" | wc -l)"
    if [[ "$NONE" != "1" ]]; then
        echo "NeoVim already installed, skipping installation..."
    else
        echo "NeoVim not found on the system..."
        #   install the program
        HAS_REPO=$(find /etc/apt/ -name *.list | xargs cat | grep neovim-ppa | wc -l)
        if [[ $HAS_REPO -eq 0 ]]; then
            #   Add repo
            sudo add-apt-repository ppa:neovim-ppa/unstable
            sudo apt-get update
        fi
        echo "Installing NeoVim"
        sudo apt-get install $UNIX_NEOVIM_ID
    fi
fi

if [ $IS_WIN -eq 1 ]; then
    #   check if program already installed; if so skip installation
    {
        winget list -e --id $WIN_NEOVIM_ID
    } &> /dev/null
    if [ $? == 0 ]; then
        echo "NeoVim already installed, skipping installation..."
    else
        echo "NeoVim not found on the system..."
        #   install the program
        echo "Installing NeoVim"
        mkdir -p "$INSTALL_DIR"
        #   windows programs expect backlash instead of slash in paths
        INSTALL_ROOT="$(echo $INSTALL_DIR | tr -s / | tr / \\\\)"
        winget install $WIN_NEOVIM_ID --uninstall-previous --accept-source-agreements --location "$INSTALL_ROOT" --custom "INSTALL_ROOT=$INSTALL_ROOT"
    fi
fi

echo "Setting up Python environment..."
if [ $IS_UNIX -eq 1 ]; then
    DEBUGPY_PATH=$UNIX_HOME/.virtualenvs/debugpy/bin/python
    mkdir -p "$UNIX_HOME"/.virtualenvs
    if [ ! -d "${UNIX_HOME}/.virtualenvs/debugpy" ]; then
        echo "Setting up debugpy virtual environment..."
        python -m venv --system-site-packages "$UNIX_HOME"/.virtualenvs/debugpy
    else
        echo "Debugpy virtual environment already exist"
    fi
    DEBUGPY_INSTALLED=$($DEBUGPY_PATH -m pip list | grep debugpy | wc -l)
    if [ "$DEBUGPY_INSTALLED" != "0" ]; then
        echo "Debugpy already installed in virtual environment"
    else
        echo "Installing debugpy in virtual environment"
        $DEBUGPY_PATH -m pip install debugpy &> /dev/null
    fi
fi

#   check if neovim config exists already, in that case replace it with default
if [[ -f "$NEOVIM_CONFIG_PATH" && -d "$NEOVIM_CONFIG" && "" != "$NEOVIM_CONFIG" ]]; then
    echo "Found existing config, removing it..."
    rm -rf $NEOVIM_CONFIG
else
    echo "No existing config found..."
fi

echo "Adding config..."
mkdir -p "$NEOVIM_CONFIG"
{
VIMSHELL="bash"
if [ $IS_WIN -eq 1 ]; then VIMSHELL="bash.exe"; fi
cat <<EOF | tee $NEOVIM_CONFIG_PATH
vim.g.mapleader = " "
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.scrolloff = 8
vim.opt.termguicolors = true
vim.opt.fileformat="unix"

require("init.lazy")

vim.opt.tabstop=8
vim.opt.softtabstop=8
vim.opt.shiftwidth=4
vim.opt.smarttab=true
vim.opt.expandtab=true

vim.opt.number=true
vim.opt.relativenumber=true

vim.opt.shell="$VIMSHELL"
vim.opt.shellcmdflag = "-c"
vim.opt.shellredir = ">%s 2>&1"
vim.opt.shellquote= ""
vim.opt.shellxescape= ""
-- set noshelltemp
vim.opt.shellxquote= ""
vim.opt.shellpipe="2>&1| tee"

require("postinit.nvimdap")
require("postinit.telescope")
require("postinit.lspzero")
require("postinit.colorscheme")
require("postinit.nvimtree")
require("postinit.tasks")
require("postinit.undotree")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("n", "<leader>gs", function() vim.api.nvim_command(":sp | :" .. vim.v.count) end)
vim.keymap.set("n", "<leader>gv", function() vim.api.nvim_command(":vs | :" .. vim.v.count) end)
vim.keymap.set("n", "<leader>gt", function() vim.api.nvim_command(":0tab sb | :" .. vim.v.count) end)
vim.keymap.set("n", "<leader>cd", function() vim.api.nvim_command("cd %:h") end)
vim.keymap.set("t", "<Esc>", "<C-\\\\><C-n>")
EOF

mkdir -p "$NEOVIM_CONFIG_LUA_INIT"
if [ -f "${NEOVIM_CONFIG_LUA_INIT}/lazy.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_INIT}/lazy.lua"
fi
cat <<EOF | tee $NEOVIM_CONFIG_LUA_INIT/lazy.lua
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({{
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function () 
      local configs = require("nvim-treesitter.configs")

      configs.setup({
          ensure_installed = { "bash", "c", "cpp", "lua", "vim", "vimdoc", "query", "javascript", "html", "json", "yaml", "python" },
          sync_install = false,
          highlight = { enable = true },
          indent = { enable = true },  
        })
    end
 },
 {
     "nvim-telescope/telescope.nvim",
     tag = "0.1.5",
     dependencies = { "nvim-lua/plenary.nvim" }
 },
 -- DAP
 {
    "rcarriga/nvim-dap-ui",
    dependencies = {
        "mfussenegger/nvim-dap",
        "nvim-neotest/nvim-nio"
    }
 },
 {
    "jay-babu/mason-nvim-dap.nvim",
    dependencies = {
        "williamboman/mason.nvim",
        "mfussenegger/nvim-dap"
    }
 },
 {"mfussenegger/nvim-dap"},
 {"nvim-neotest/nvim-nio"},
 {
     "mfussenegger/nvim-dap-python",
     ft = "python",
     dependencies = {
        "mfussenegger/nvim-dap",
        "rcarriga/nvim-dap-ui"
     }
 },
 -- LSP
 {"williamboman/mason.nvim"},
 {"williamboman/mason-lspconfig.nvim"},
 {"VonHeikemen/lsp-zero.nvim", branch = "v3.x"},
 {"neovim/nvim-lspconfig"},
 {"hrsh7th/cmp-nvim-lsp"},
 {"hrsh7th/nvim-cmp"},
 {"L3MON4D3/LuaSnip"},
 -- Color
 {"catppuccin/nvim", name = "catppuccin", priority = 1000},
 -- File tree
 {"nvim-tree/nvim-web-devicons"},
 {
    "nvim-tree/nvim-tree.lua",
    version = "*",
    lazy = false,
    dependencies = {
        "nvim-tree/nvim-web-devicons",
    },
    config = function()
        require("nvim-tree").setup {}
    end,
 },
 -- Tasks (CMake)
 {"Shatur/neovim-tasks"},
 -- Undotree
 {"mbbill/undotree"}
})
EOF
mkdir -p "$NEOVIM_CONFIG_LUA_POSTINIT"
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/nvimdap.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/nvimdap.lua"
fi
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/telescope.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/telescope.lua"
fi
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/lspzero.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/lspzero.lua"
fi
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/colorscheme.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/colorscheme.lua"
fi
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/nvimtree.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/nvimtree.lua"
fi
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/tasks.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/tasks.lua"
fi
if [ -f "${NEOVIM_CONFIG_LUA_POSTINIT}/undotree.lua" ]; then
    rm "${NEOVIM_CONFIG_LUA_POSTINIT}/undotree.lua"
fi
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/nvimdap.lua
local mason = require("mason")
local nvim_dap = require("dap")
local mason_nvim_dap = require("mason-nvim-dap")
local nvim_dap_ui = require("dapui")
nvim_dap_ui.setup()
nvim_dap.listeners.after.event_initialized["dapui_config"] = function()
  nvim_dap_ui.open()
end
nvim_dap.listeners.after.event_terminated["dapui_config"] = function()
  nvim_dap_ui.close()
end
nvim_dap.listeners.after.event_exited["dapui_config"] = function()
  nvim_dap_ui.close()
end
vim.keymap.set('n', '<F5>', function()
  local cwd = vim.fn.getcwd() 
  for k,v in ipairs(nvim_dap.configurations.python) do
    v.cwd = cwd
    v.env = { PYTHONPATH = cwd }
  end
  nvim_dap.continue()
end)
vim.keymap.set('n', '<F6>', function() nvim_dap.step_over() end)
vim.keymap.set('n', '<F7>', function() nvim_dap.step_into() end)
vim.keymap.set('n', '<F8>', function() nvim_dap.step_out() end)
vim.keymap.set('n', '<Leader>b', function() nvim_dap.toggle_breakpoint() end)
vim.keymap.set('n', '<Leader>B', function() nvim_dap.set_breakpoint() end)
vim.keymap.set('n', '<Leader>dr', function() nvim_dap.repl.open() end)
vim.keymap.set('n', '<Leader>dl', function() nvim_dap.run_last() end)
vim.keymap.set({'n', 'v'}, '<Leader>dh', function()
  require('dap.ui.widgets').hover()
end)
vim.keymap.set({'n', 'v'}, '<Leader>dp', function()
  require('dap.ui.widgets').preview()
end)
vim.keymap.set('n', '<Leader>df', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.frames)
end)
vim.keymap.set('n', '<Leader>ds', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.scopes)
end)

nvim_dap.adapters.codelldb = {
  type = 'server',
  port = "\${port}",
  executable = {
    -- CHANGE THIS to your path!
    command = 'codelldb.cmd',
    args = {"--port", "\${port}"},

    -- On windows you may have to uncomment this:
    -- detached = false,
  }
}

dap_python = require("dap-python")
dap_python.setup("${DEBUGPY_PATH}")
dap_python.resolve_python = function()
  return "${DEBUGPY_PATH}"
end

local function get_arguments()
  local co = coroutine.running()
  if co then
    return coroutine.create(function()
      local args = {}
      vim.ui.input({ prompt = 'Enter command-line arguments: ' }, function(input)
        args = vim.split(input, " ")
      end)
      coroutine.resume(co, args)
    end)
  else
    local args = {}
    vim.ui.input({ prompt = 'Enter command-line arguments: ' }, function(input)
      args = vim.split(input, " ")
    end)
    return args
  end
end

nvim_dap.configurations.cpp = {
  {
    name = "Launch file",
    type = "codelldb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '\${workspaceFolder}',
    stopOnEntry = false,
  },
}
nvim_dap.configurations.python = {
  {
      name = "Launch file";
      type = "python";
      request = "launch";
      program = "\${file}";
      justMyCode = false;
  },
  {
      name = "Launch file with arguments";
      type = "python";
      request = "launch";
      program = "\${file}";
      justMyCode = false;
      args = get_arguments;
  },
}
EOF
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/telescope.lua
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>pf", builtin.find_files, {})
vim.keymap.set("n", "<C-p>", builtin.git_files, {})
vim.keymap.set("n", "<leader>ps", function()
        builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)
vim.keymap.set("n", "<leader>jt", function() builtin.lsp_definitions({jump_type="tab"}) end, {noremap=true, silent=true})
vim.keymap.set("n", "<leader>jv", function() builtin.lsp_definitions({jump_type="vsplit"}) end, {noremap=true, silent=true})
vim.keymap.set("n", "<leader>js", function() builtin.lsp_definitions({jump_type="split"}) end, {noremap=true, silent=true})
EOF
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/lspzero.lua
local mason = require("mason")
mason.setup({})

local mason_lspconfig = require("mason-lspconfig")
mason_lspconfig.setup({
  ensure_installed = {
    --"awk_ls",
    "bashls",
    "clangd",
    "cmake",
    "cssls",
    "eslint",
    "helm_ls",
    "html",
    "jsonls",
    "lua_ls",
    "pylsp",            -- Python LSP
    "tsserver",
    "yamlls"
  }
})

local lsp_zero = require("lsp-zero")
lsp_zero.on_attach(function(client, bufnr)
  local opts = {buffer = bufnr, remap = false}

  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
  vim.keymap.set('n', 'gD', function()
        local org_path = vim.api.nvim_buf_get_name(0)

        -- Go to definition:
        vim.api.nvim_command('normal gd')

        -- Wait LSP server response
        vim.wait(100, function() end)

        local new_path = vim.api.nvim_buf_get_name(0)
        if not (org_path == new_path) then
            -- Create a new tab for the original file
            vim.api.nvim_command('0tabnew ' .. org_path)

            -- Restore the cursor position
            vim.api.nvim_command('b ' .. org_path)
            vim.api.nvim_command('normal! \`"')

            -- Switch to the original tab
            vim.api.nvim_command('normal! gt')
        end
    end, bufopts)
  vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
  vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
  vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
  vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
  vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
  vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
  vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
  vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)
  vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)
  vim.api.nvim_buf_set_keymap(bufnr, "n", "<space>f", "<cmd>lua vim.lsp.buf.format()<CR>", {})
end)

setup_handlers = {
    pylsp = { 
        settings = { 
            pylsp = { 
                plugins = { 
                    jedi = { 
                        environment = "/usr/bin/python",
                    } 
                } 
            } 
        } 
    }
}

mason_lspconfig.setup_handlers({
    function (server_name)
      handler = setup_handlers[server_name] 
      require("lspconfig")[server_name].setup(handler == nil and {} or handler)
    end,
  })

local mason_registry = require("mason-registry")
local mason_package = require("mason-core.package")

local ensure_installed_manually = {
    "bzl",
    "clang-format",
    "codelldb",
    "black",            -- Python linter
    "isort",            -- Python linter
    "mypy",             -- Python linter
    "ruff",             -- Python linter
  }

local function install_packages_manually()
  for _, v in ipairs(ensure_installed_manually) do
    if not mason_registry.is_installed(v) then
      pkg = mason_registry.get_package(v)
      pkg:install({})
    end
  end
end

if mason_registry.refresh then
  mason_registry.refresh(vim.schedule_wrap(install_packages_manually))
else
  install_packages_manually()
end

local cmp = require("cmp")
local cmp_select = {behavior = cmp.SelectBehavior.Select}

cmp.setup({
  sources = {
    {name = "path"},
    {name = "nvim_lsp"},
    {name = "nvim_lua"},
  },
  formatting = lsp_zero.cmp_format(),
  mapping = cmp.mapping.preset.insert({
    ["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
    ["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
    ["<C-y>"] = cmp.mapping.confirm({ select = true }),
    ["<C-Space>"] = cmp.mapping.complete(),
  }),
})
EOF
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/colorscheme.lua
require("catppuccin").setup({
    flavour = "frappe", -- latte, frappe, macchiato, mocha
    background = { -- :h background
        light = "latte",
        dark = "mocha",
    },
    transparent_background = false, -- disables setting the background color.
    show_end_of_buffer = false, -- shows the '~' characters after the end of buffers
    term_colors = false, -- sets terminal colors (e.g. `g:terminal_color_0`)
    dim_inactive = {
        enabled = false, -- dims the background color of inactive window
        shade = "dark",
        percentage = 0.15, -- percentage of the shade to apply to the inactive window
    },
    no_italic = false, -- Force no italic
    no_bold = false, -- Force no bold
    no_underline = false, -- Force no underline
    styles = { -- Handles the styles of general hi groups (see `:h highlight-args`):
        comments = { "italic" }, -- Change the style of comments
        conditionals = { "italic" },
        loops = {},
        functions = {},
        keywords = {},
        strings = {},
        variables = {},
        numbers = {},
        booleans = {},
        properties = {},
        types = {},
        operators = {},
    },
    color_overrides = {},
    custom_highlights = {},
    integrations = {
        cmp = true,
        gitsigns = true,
        nvimtree = true,
        treesitter = true,
        notify = false,
        mini = {
            enabled = true,
            indentscope_color = "",
        },
        -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
})

-- setup must be called before loading
vim.cmd.colorscheme "catppuccin"
EOF
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/nvimtree.lua
local function attach_to_nvimtree(bufnr)
  local api = require "nvim-tree.api"

  local function opts(desc)
    return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  -- default mappings
  api.config.mappings.default_on_attach(bufnr)

  -- custom mappings
  vim.keymap.set("n", "<C-t>",      api.tree.change_root_to_parent,        opts("Up"))
  vim.keymap.set("n", "?",          api.tree.toggle_help,                  opts("Help"))
end

require("nvim-tree").setup({
  sort = {
    sorter = "case_sensitive",
  },
  view = {
    width = 35,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
  on_attach = attach_to_nvimtree
})

local api = require("nvim-tree.api")
vim.keymap.set("n", "<leader>l", api.tree.toggle)
EOF
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/tasks.lua
local Path = require('plenary.path')
require('postinit.nvimdap')
require('tasks').setup({
  default_params = { -- Default module parameters with which `neovim.json` will be created.
    cmake = {
      cmd = 'cmake', -- CMake executable to use, can be changed using `:Task set_module_param cmake cmd`.
      build_dir = tostring(Path:new('{cwd}', 'build', '{os}-{build_type}')), -- Build directory. The expressions `{cwd}`, `{os}` and `{build_type}` will be expanded with the corresponding text values. Could be a function that return the path to the build directory.
      build_type = 'Debug', -- Build type, can be changed using `:Task set_module_param cmake build_type`.
      dap_name = 'codelldb', -- DAP configuration name from `require('dap').configurations`. If there is no such configuration, a new one with this name as `type` will be created.
      args = { -- Task default arguments.
        configure = { '-D', 'CMAKE_EXPORT_COMPILE_COMMANDS=1', '-G', 'Ninja' },
      },
      env = {
        configure = {"CC=clang.exe", "CXX=clang++.exe"}
      },
    },
  },
  save_before_run = true, -- If true, all files will be saved before executing a task.
  params_file = 'neovim.json', -- JSON file to store module and task parameters.
  quickfix = {
    pos = 'botright', -- Default quickfix position.
    height = 12, -- Default height.
  },
  dap_open_command = function() return require('dap').repl.open() end, -- Command to run after starting DAP session. You can set it to `false` if you don't want to open anything or `require('dapui').open` if you are using https://github.com/rcarriga/nvim-dap-ui
})
EOF
cat <<EOF | tee $NEOVIM_CONFIG_LUA_POSTINIT/undotree.lua
vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
EOF
} &> /dev/null